
function changeBackground(){
    var div = document.getElementsByClassName("change-color");
    for (var i = 0; i < div.length; i++){
        if ((i + 1) % 2 == 0){
            div[i].style.background = "red";
        }
        else { 
            div[i].style.background = "blue";
        }
    }
}